@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Task') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="{{ route('task.create') }}" type="button" class="btn btn-danger">Create new task</a>

                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Title</th>
                            <th scope="col">Description</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($tasks as $task)
                            <tr>
                              <th scope="row">{{ $loop->iteration }}</th>
                              <td>{{ $task->title }}</td>
                              <td>{{ $task->description }}</td>
                              <td><a onclick="return confirm('Are you sure to delete task?')" href="{{ route('task.destroy', $task) }}" class="btn btn-danger">Delete</a></td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
