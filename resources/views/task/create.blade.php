@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Task') }}</div>

                <div class="card-body">
                    <form method="post" action="{{ route('task.store') }}">
                        @csrf
                        <div class="form-group">
                          <label for="title">Title</label>
                          <input type="text" class="form-control" id="title" name="title" placeholder="Add title">
                        </div>
                        <div class="form-group">
                            <label for="title">Description</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" name="description" rows="5">
                            </textarea>                          
                        </div>

                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
