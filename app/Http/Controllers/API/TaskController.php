<?php

namespace App\Http\Controllers\API;

use App\Models\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    public function store(Request $request)
    {
        $task = Task::create([
            'title' => $request->title,
            'description' => $request->description,
        ]);

        return response()->json([
            'success' => true,
            'data' => $task,
            'message' => 'Task created successfully'
        ]);
    }

    public function show(Request $request)
    {
        $task = Task::all();

        return response()->json([
            'success' => true,
            'message' => 'Task show',
            'data' => $task,
        ]);
    }
}
