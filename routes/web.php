<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::controller(TaskController::class)->group(function(){
    Route::get('/task/index','index')->name('task.index');
    Route::get('/task/create','create')->name('task.create');
    Route::post('/task/store','store')->name('task.store');
    Route::get('/task/delete/{task}','destroy')->name('task.destroy');
});



